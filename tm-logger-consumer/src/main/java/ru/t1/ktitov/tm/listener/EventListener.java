package ru.t1.ktitov.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public final class EventListener implements MessageListener {

    private final LoggerService loggerService;

    public EventListener(@NotNull final LoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof TextMessage)) return;
        @NotNull final String json = ((TextMessage) message).getText();
        loggerService.log(json);
    }

}
